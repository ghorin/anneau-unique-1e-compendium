import {Tor1eCompendiumCaracteristiques} from "./scripts/caracteristiques.js";
import {Tor1eCompendiumEquipement} from "./scripts/equipement.js";

export class Tor1eCompendium {
    constructor() {
        this.mesCaracteristiques = new Tor1eCompendiumCaracteristiques();
        this.monEquipement = new Tor1eCompendiumEquipement();
    }

    // INITIALISATION OF CHARACTER CARACTERISTICS
    async initCaracteristiques() {
        console.log("=== Tor1eCompendium : CARAC DEBUT ===");
        await this.mesCaracteristiques.creationCaracteristiques();
        console.log("=== Tor1eCompendium : CARAC FIN ===");
    }

    // INTIALISATION OF EQUIPMENT ITEMS
    async initEquipement() {
        console.log("=== Tor1eCompendium : EQUIPEMENT DEBUT ===");
        await this.monEquipement.creationEquipement();
        console.log("=== Tor1eCompendium : EQUIPEMENT FIN ===");
    }

    // INITIALISATION OF THE ENTIRE WORLD (caracteristics, items, adversaries)
    async initWorld() {
        ui.notifications.notify('===== DEBUT CREATION DU COMPENDIUM ====');
        await this.initCaracteristiques();
        await this.initEquipement();
        ui.notifications.notify('===== FIN CREATION DU COMPENDIUM ====');
    }
}

Hooks.once('init', async function () {
    game.tor1eCompendium = new Tor1eCompendium();
    console.log("=== Tor1eCompendium : Initialisation ===");
});
