# COMPENDIUM POUR L'ANNEAU UNIQUE 1ERE EDITION DANS FOUNDRY VTT

## Compatibilité
- Foundry VTT 0.7.x
- Foundry VTT 0.8.x

## CONTENU

Ce compendium contient tous les éléments nécessaires pour créer des personnages joueurs, non joueurs et adversaires dans le système TOR1E de Foundry VTT.

- Compétences,
- Traits : Spécialités et Particularités
- Récompenses et Vertus
- Défauts d'Ombre
- Capacités Spéciales
- Equipement martial (armes, armures, casques)

Langue française uniquement.

Chaque élément est décrit par son nom, des statistiques le cas échéant (pour l'équipement martial tout particulièrement) et par une référence aux livres (nom du livre + numéro de page) pour y trouver la description complète et officielle.

Armes des adversaires : Les livres contiennent énormément d'armes différentes (statistiques différentes) avec un nom identique. Les livres contiennent également nombre d'armes équivalentes avec un nom similaire mais différent. Ce compendium ne contient qu'une petite partie de cette diversité d'armes des adversaires, chaque Gardien des Légendes utilisant ce compendium devra compléter / modifier ces armes selon son besoin.

<img src="http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-Compendium-1.jpg"/>


## LICENCE ET DROIT D'USAGE

L’Anneau Unique, La Terre du Milieu, Le Hobbit, Le Seigneur des Anneaux, et les personnages, objets, événements et lieux qu’on y trouve sont des marques déposées de The Saul Zaentz Company d/b/a Middle-earth Enterprises et sont utilisés par Sophisticated Games Ltd sous licence. Tous droits réservés à leurs propriétaires respectifs. Édition française par Edge Studio.

Le contenu de ce compendium NE CONTIENT AUCUNE image ni texte descriptif provenant des livres édités par Edge Studio (VF) ou par Cubicle7 (VO). Il n'a pour objectif que d'aider les joueurs à préparer leur sessions de jeu dans Foundry VTT. Les joueurs devront utiliser leurs propres livres (format papier ou pdf) de l'Anneau Unique afin de compléter les informations manquantes et pouvoir les utiliser.

Merci de soutenir les créateurs et éditeurs de nos jeux : Vous pouvez vous procurer l'Anneau Unique dans les magasins près de chez vous et sur les sites marchands. L'ensemble des PDFs de l'Anneau Unique sont disponibles sur [drivethrurpg.com](https://www.drivethrurpg.com/browse.php?keywords=l%27anneau+unique&manufacturers_id=17946&x=0&y=0&author=&artist=&pfrom=&pto=)

Ce compendium n'est pas officiel : il n'est pas fournit et maintenu par Edge Studio. Il est mis à disposition gratuitement et aucune revente n'est autorisée.



## PRE-REQUIS

Ce compendium ne fonctionne qu'avec le système dédié à l'Anneau Unique 1ère édition dans Foundry VTT : [The One Ring 1st edition](https://foundryvtt.com/packages/tor1e)


## INSTALLATION DU MODULE

Par le lien du Manifest : [https://gitlab.com/ghorin/anneau-unique-1e-compendium/-/raw/main/module.json](https://gitlab.com/ghorin/anneau-unique-1e-compendium/-/raw/main/module.json)


## INTEGRATION DU CONTENU DANS LA PARTIE

Il y a 2 moyens d'intégrer le contenu du Compendium dans votre partie :

### 1) Mode Utilisateur : Glisser & déposer du Compendium vers l'onglet Objets
1. Se connecter à la partie dans Foundry VTT
2. Activer le module _L'Anneau Unique - Le Compendium (fr)_ 
3. Aller dans l'onglet Compendium, vous y trouverez un compendium pour chaque type d'objet des feuilles de personnages. Pour chaque compendium, faire un clic-droit puis choisissez l'option "Importer tout le contenu" en précisant le dossier de destination. Le contenu est ensuite disponible dans l'onglet Objets

### 2) Mode Geek : Création (par une macro) dans l'onglet Objets
1. Se connecter à la partie dans Foundry VTT
2. Activer le module _L'Anneau Unique - Le Compendium (fr)_ 
3. Créer une nouvelle macro ou cliquant dans une case vide de votre Hotbar et remplissez la fenêtre par les éléments suivants :
    - Type : Script
    - Command : game.tor1eCompendium.initWorld();
4. Exécuter la macro (note : activer la console via la touche F12 pour voir le déroulement de la macro)


## UTILISATION DU CONTENU

Lorsque le contenu a été importé, il est disponible dans l'onglet Objets et chaque item peut être drag'n droppé dans une feuille de personnage.
Le Gardien des légendes peut ensuite modifier chaque item à sa convenance ou en créer de nouveaux. C'est le cas tout particulièrement pour les armes des adversaires car ce compendium n'en contient qu'une petite partie.


## ERREURS ET AMELIORATION

Des erreurs peuvent s'être glissées dans ce compendium. Si vous en découvrez, merci de les signaler en créant un "ticket" dans la page suivante : [Tickets (issues)](https://gitlab.com/ghorin/anneau-unique-1e-compendium/-/issues)



## QUELQUES LIENS

- [Foundry VTT](https://foundryvtt.com/)
- [Système TOR1E pour l'Anneau Unique 1ère édition dans Foundry VTT](https://foundryvtt.com/packages/tor1e)
- [Chaine de l'Anneau Unique dans le serveur Discord "La Fonderie" (communauté FR dédiée à Foundry VTT)](https://discord.com/channels/715943353409339425/807665041473929256)
- [Aides de jeu sur l'Anneau Unique + 1 page dédiée à L'Anneau unique dans Foundry VTT](http://ostolinde.free.fr/au_aidesdejeu.html)
